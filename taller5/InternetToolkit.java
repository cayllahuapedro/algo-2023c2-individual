package aed;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class InternetToolkit {

    public Fragment[] tcpReorder(Fragment[] fragments) {
        // IMPLEMENTA

        int n = fragments.length;

        for (int i = 1; i < n; i++) {
            Fragment current = fragments[i];
            int j = i - 1;

            while (j >= 0 && fragments[j].compareTo(current) > 0) {
                fragments[j + 1] = fragments[j];
                j--;
            }
            fragments[j + 1] = current;
        }

        return fragments;
    }

    private void swap(Router[] routers, int i, int j) {
        Router temp = routers[i];
        routers[i] = routers[j];
        routers[j] = temp;

    }

    private void buildHeap(Router[] routers) {
        int n = routers.length;
        for (int i = (n / 2) - 1; i >= 0; i--) {
            heapify(routers, i, n);
        }
    }

    // 0,1,2,3,4,5,6,7,8,9
    private void heapify(Router[] routers, int i, int n) {
        int left = 2 * i + 1;
        int right = 2 * i + 2;
        int max = i;

        if (left < n && routers[left].compareTo(routers[max]) > 0) {
            max = left;
        }

        if (right < n && routers[right].compareTo(routers[max]) > 0) {
            max = right;
        }

        if (max != i) {
            swap(routers, i, max);
            heapify(routers, max, n);
        }
    }

    public Router[] kTopRouters(Router[] routers, int k, int umbral) {
        // IMPLEMENTAR
        // filtrar por umbral, crando un array list. O(n)
        ArrayList<Router> routersFiltrado = new ArrayList<>();
        Router umbralRouter = new Router(0, umbral);
        for (Router element : routers) {
            if (element.compareTo(umbralRouter) > 0) {
                routersFiltrado.add(element);
            }
        }
        // pasar de arrayList a array. O(n)
        Router[] routers2 = new Router[routersFiltrado.size()];
        for (int i = 0; i < routersFiltrado.size(); i++) {
            routers2[i] = routersFiltrado.get(i);
        }

        // construir un heap a partir del array
        int n = routers2.length;
        buildHeap(routers2);
        // heap sort
        // Ordenar de manera decreciente (heap sort)
        for (int i = n - 1; i > 0; i--) {
            swap(routers2, 0, i);
            heapify(routers2, 0, i);
        }

        // Obtener los k elementos finales en orden decreciente
        Router[] result = new Router[Math.min(k, routers2.length)];
        for (int i = 0; i < result.length; i++) {
            result[i] = routers2[n - i - 1];
        }

        return result;
    }

    // Router[] res= new Router[k];
    // for (int i=0; i<k;i++){
    // res[i]=routers2[i];
    // }

    private void swap(IPv4Address[] ipv4, int i, int j) {
        IPv4Address temp = ipv4[i];
        ipv4[i] = ipv4[j];
        ipv4[j] = temp;
    }

    public IPv4Address[] sortIPv4(String[] ipv4) {
        // IMPLEMENTAR
        // crear una lista de objetos Ipv4
        int n = ipv4.length;
        IPv4Address[] res = new IPv4Address[n];
        for (int i = 0; i < n; i++) {
            IPv4Address Ipv4 = new IPv4Address(ipv4[i]);
            res[i] = Ipv4;
        }

        for (int i = 0; i < res.length; i++) {
            for (int j = i + 1; j < res.length; j++) {
                if (res[i].getOctet(0) > res[j].getOctet(0)) {
                    swap(res, i, j);
                } else if (res[i].getOctet(0) == res[j].getOctet(0)) {
                    if (res[i].getOctet(1) > res[j].getOctet(1)) {
                        swap(res, i, j);
                    } else if (res[i].getOctet(1) == res[j].getOctet(1)) {
                        if (res[i].getOctet(2) > res[j].getOctet(2)) {
                            swap(res, i, j);
                        } else if (res[i].getOctet(2) == res[j].getOctet(2)) {
                            if (res[i].getOctet(3) > res[j].getOctet(3)) {
                                swap(res, i, j);
                            }

                        }
                    }
                }
                    }
            }
            return res;
        }
}
