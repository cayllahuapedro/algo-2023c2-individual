package aed;

class Funciones {
    int cuadrado(int x) {
        // COMPLETAR
        int res=x*x;
        return res;
    }

    double distancia(double x, double y) {
        // COMPLETAR
        double res=Math.sqrt(x*x+y*y);
        return res;
    }

    boolean esPar(int n) {
        // COMPLETAR\
        boolean res;
        if (n%2==0){
            res=true; 
        }
        else{
            res=false;
        }
        return res;  
    }

    boolean esBisiesto(int n) {
        // COMPLETAR
        boolean res;
        if(n%100!=0 && n%4==0 || n%400==0){
            res=true;
        } else{
            res=false;
        }
        return res;
    }

    int factorialIterativo(int n) {
        // COMPLETAR
        int res=1;
        if (n==0){
            res=1;
        } else{
              for(int i=1; i<=n; i++){
            res=res*i;
              }
        }
      
        return res;
    }

    int factorialRecursivo(int n) {
        // COMPLETAR
        int res=1;
        if (n==0 ||n==1){
            return 1;
        }
        else{
            res=factorialRecursivo(n-1)*n;
        }
        return res;
    }

    boolean esPrimo(int n) {
        // COMPLETAR
        boolean res=true;
        if(n==0 || n==1){
            res=false;
            return res;
        }
        for (int i=2;i<n;i++){
            if (n%i==0){
                res=false;
            } //si i divide a n
        }
        return res;
    }

    int sumatoria(int[] numeros) {
        // COMPLETAR
        int res=0;
        for (int i=0;i<numeros.length ;i++){
            res= res+ numeros[i];
        }
        return res;
    }

    int busqueda(int[] numeros, int buscado) {
        // COMPLETAR
        int i=0;
        while(i<numeros.length){
            if(numeros[i]==buscado){
                return i;
            }
            i++;
        }
        return 0;
    }

    boolean tienePrimo(int[] numeros) {
        // COMPLETAR
        int i=0;
        while(i<numeros.length){
            if(esPrimo(numeros[i])==true){
                return true;
            }
            i++;
        }
        return false;
    }

    boolean todosPares(int[] numeros) {
        // COMPLETAR
        int i=0;
        while(i<numeros.length){
            if (numeros[i]%2!=0){
                return false;
            }
            i++;
        }
        return true;
    }

    boolean esPrefijo(String s1, String s2) {
        // COMPLETAR
        boolean res=true;
        if(s1.length()<=s2.length()){
            for (int i=0; i<s1.length(); i++){
                if(s1.charAt(i)!= s2.charAt(i)){
                    return false;
                }
            }
        }
        else{
            res=false;
        }
        return res;
    }

    boolean esSufijo(String s1, String s2) {
        // COMPLETAR
        boolean res=true;
        if(s1.length()<=s2.length()){
            for (int i=0; i<s1.length(); i++){
                if(s1.charAt(i)!=s2.charAt((s2.length()-s1.length())+i)){
                    return false;
                }
            }
        }
        else {
                return false;
            }

        return res;
    }
}
