package aed;

import java.util.*;

// Todos los tipos de datos "Comparables" tienen el método compareTo()
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    // Agregar atributos privados del Conjunto
    private Nodo raiz;
    private int cardinal;

    private class Nodo {
        // Agregar atributos privados del Nodo
        private T elemento;
        private Nodo hijoMenor;
        private Nodo hijoMayor;

        // Crear Constructor del nodo
        private Nodo() {
            elemento = null;
            hijoMayor = null;
            hijoMenor = null;
        }
    }

    public ABB() {
        cardinal = 0;
        raiz = null;
    }

    public int cardinal() {
        return cardinal;
    }

    public boolean pertenece(T elem) {
        Nodo nodo_actual = raiz;

        while (nodo_actual != null) {
            if (nodo_actual.elemento.compareTo(elem) == 0) {
                return true;
            } else if (nodo_actual.elemento.compareTo(elem) > 0) { // nodo> elem
                nodo_actual = nodo_actual.hijoMenor;
            } else { // nodo< elem
                nodo_actual = nodo_actual.hijoMayor;
            }
        }

        return false;
    }

    public T minimo() {
        if (raiz == null) {
            return null;
        } else {
            Nodo nodo_actual = raiz;
            while (true) {
                if (nodo_actual.hijoMenor == null) {
                    return nodo_actual.elemento;
                } else {
                    nodo_actual = nodo_actual.hijoMenor;
                }
            }
        }

    }

    public T maximo() {
        if (raiz == null) {
            return null;
        } else {
            Nodo nodo_actual = raiz;
            while (true) {
                if (nodo_actual.hijoMayor == null) {
                    return nodo_actual.elemento;
                } else {
                    nodo_actual = nodo_actual.hijoMayor;
                }
            }
        }
    }

    public void insertar(T elem) {
        // aca trate de iterar sobre el arbol comparando con el nodo, cuando me encontre
        // un null
        // luego iguale actual y nuevo para salir del ciclo while y tambien para que ese
        // nuevo nodo
        // vaya a esa posicion en particular
        // al parecer funcion bien, lo unico tengo problemas para manejar el caso donde
        // el elem ya pertenece al conjunto
        // o es una falla de este codigo o de la funcion pertenece
        Nodo nodo_nuevo = new Nodo();
        nodo_nuevo.elemento = elem;
        Nodo nodo_actual = raiz;
        if (raiz == null) {
            raiz = nodo_nuevo;
            cardinal++;
            // } else if (pertenece(elem)) {
            // ;
        } else {
            while (true) {
                if (nodo_actual.elemento.compareTo(elem) > 0) { // elem < nodo
                    if (nodo_actual.hijoMenor == null) {
                        nodo_actual.hijoMenor = nodo_nuevo;
                        cardinal++;
                        break;
                    } else {
                        nodo_actual = nodo_actual.hijoMenor;
                    }

                } else if (nodo_actual.elemento.compareTo(elem) < 0) { // elem > nodo
                    if (nodo_actual.hijoMayor == null) {
                        nodo_actual.hijoMayor = nodo_nuevo;
                        cardinal++;
                    } else {
                        nodo_actual = nodo_actual.hijoMayor;
                    }
                } else {
                    break;
                }
            }
        }
    }

    public void eliminar(T elem) {
        if (!pertenece(elem)) {
            return; // El elemento no está en el árbol, no hay nada que eliminar.
        }

        raiz = eliminarRecursivo(raiz, elem);
        cardinal--;
    }

    private Nodo eliminarRecursivo(Nodo nodo, T elem) {
        if (nodo == null) {
            return nodo;
        }

        int comparacion = elem.compareTo(nodo.elemento);

        if (comparacion < 0) {
            nodo.hijoMenor = eliminarRecursivo(nodo.hijoMenor, elem);
        } else if (comparacion > 0) {
            nodo.hijoMayor = eliminarRecursivo(nodo.hijoMayor, elem);
        } else {
            // Caso 1: El nodo no tiene hijos o solo uno
            if (nodo.hijoMenor == null) {
                return nodo.hijoMayor;
            } else if (nodo.hijoMayor == null) {
                return nodo.hijoMenor;
            }

            // Caso 2: El nodo tiene dos hijos
            nodo.elemento = minimoValor(nodo.hijoMayor);

            // Eliminar el sucesor en orden
            nodo.hijoMayor = eliminarRecursivo(nodo.hijoMayor, nodo.elemento);
        }

        return nodo;
    }

    private T minimoValor(Nodo nodo) {
        T minimo = nodo.elemento;
        while (nodo.hijoMenor != null) {
            minimo = nodo.hijoMenor.elemento;
            nodo = nodo.hijoMenor;
        }
        return minimo;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        construirStringEnOrden(raiz, sb);
        if (sb.length() >= 2) {
            // Eliminamos la coma y el espacio en blanco al final de la cadena
            sb.setLength(sb.length() - 1);
        }
        sb.append("}");
        return sb.toString();
    }

    private void construirStringEnOrden(Nodo nodo, StringBuilder sb) {
        if (nodo != null) {
            construirStringEnOrden(nodo.hijoMenor, sb);
            sb.append(nodo.elemento);
            sb.append(",");
            construirStringEnOrden(nodo.hijoMayor, sb);
        }
    }

    private class ABB_Iterador implements Iterador<T> {
        private Stack<Nodo> pila;
        private Nodo nodoActual;
    
        public ABB_Iterador() {
            pila = new Stack<>();
            nodoActual = raiz;
            avanzarHastaElementoMinimo();
        }
    
        public boolean haySiguiente() {
            return !pila.isEmpty();
        }
    
        public T siguiente() {
            if (!haySiguiente()) {
                throw new NoSuchElementException("No hay más elementos en el ABB.");
            }
    
            Nodo nodoSiguiente = pila.pop();
            nodoActual = nodoSiguiente.hijoMayor;
            avanzarHastaElementoMinimo();
    
            return nodoSiguiente.elemento;
        }
    
        private void avanzarHastaElementoMinimo() {
            while (nodoActual != null) {
                pila.push(nodoActual);
                nodoActual = nodoActual.hijoMenor;
            }
        }
    }

    public Iterador<T> iterador() {
        return new ABB_Iterador();
    }

}
