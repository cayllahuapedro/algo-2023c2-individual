package aed;

import java.util.*;

public class ListaEnlazada<T> implements Secuencia<T> {
    // Completar atributos privados
    private Nodo raiz;
    private int size;

    private class Nodo {
        T elemento;
        Nodo anterior;
        Nodo siguiente;

        Nodo(T valor) {
            elemento = valor;
            anterior = null;
            siguiente = null;
        }
    }

    public ListaEnlazada() {
        raiz = null;
        size = 0;
    }

    public int longitud() {
        return size;
    }

    public void agregarAdelante(T elem) {
        Nodo nuevo = new Nodo(elem);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            Nodo aux = raiz;
            nuevo.siguiente = aux;
            aux.anterior = nuevo;

            raiz = nuevo;
        }
        size++;

    }

    public void agregarAtras(T elem) {
        Nodo nodo_actual = raiz;
        Nodo nodo_nuevo = new Nodo(elem);
        if (raiz == null) {
            raiz = nodo_nuevo;
        } else {
            while (nodo_actual.siguiente != null) {
                nodo_actual = nodo_actual.siguiente;
            }
            nodo_actual.siguiente = nodo_nuevo;
            nodo_nuevo.anterior = nodo_actual;

        }
        size++;
    }

    public T obtener(int i) {
        if (size == 1) {
            return raiz.elemento;
        } else {
            Nodo nodo_actual = raiz;
            int j = 0;
            while (j < i) {
                nodo_actual = nodo_actual.siguiente;
                j++;
            }
            return nodo_actual.elemento;
        }
    }

    public void eliminar(int i) {
        if (i == 0) {
            raiz = raiz.siguiente;
        } else if (i == size - 1) {
            Nodo nodo_actual = raiz;
            while (nodo_actual.siguiente != null) {
                nodo_actual = nodo_actual.siguiente;
            }
            nodo_actual.anterior = null;
        } else {
            Nodo nodo_actual = raiz;
            int j = 0;
            while (j < i) {
                nodo_actual = nodo_actual.siguiente;
                j++;
            }

            nodo_actual.anterior.siguiente = nodo_actual.siguiente;
            nodo_actual.siguiente.anterior = nodo_actual.anterior;

        }

        size--;
    }

    public void modificarPosicion(int indice, T elem) {
        Nodo nodo_actual = raiz;
        int j = 0;
        while (j < indice) {
            nodo_actual = nodo_actual.siguiente;
            j++;
        }
        nodo_actual.elemento = elem;

    }

    public ListaEnlazada<T> copiar() {
        ListaEnlazada<T> lista_copiada = new ListaEnlazada<>();
        int i = 0;
        while (i < size) {
            lista_copiada.agregarAtras(obtener(i));
            i++;
        }
        return lista_copiada;
    }

    public ListaEnlazada(ListaEnlazada<T> lista) {
        int i = 0;
        while (i < lista.size) {
            agregarAtras(lista.obtener(i));
            i++;
        }
    }

    @Override
    public String toString() {
        ArrayList <String> arrayAux= new ArrayList<>();
        int i=0;
        while(i<size){
            arrayAux.add(String.valueOf(obtener(i)));
            i++;
        }
        String ListaImpresa = String.join(", ", arrayAux);

        return "["+ ListaImpresa +"]";

        












    }

    private class ListaIterador implements Iterador<T> {
        // Completar atributos privados
        private int dedito;

        ListaIterador() {
            dedito = 0;
        }

        public boolean haySiguiente() {
            return dedito < size;

        }

        public boolean hayAnterior() {
            return dedito > 0;
        }

        public T siguiente() {
            dedito++;
            return obtener(dedito - 1);

        }

        public T anterior() {
            dedito--;
            return obtener(dedito);

        }
    }

    public Iterador<T> iterador() {
        return new ListaIterador();
    }

}
