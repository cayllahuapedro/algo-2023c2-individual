package aed;

import java.util.Scanner;
import java.io.PrintStream;

class Archivos {
    float[] leerVector(Scanner entrada, int largo) {
        float res[];
        res=new float[largo];
        int i=0;
        if(!entrada.hasNext()){return new float[0];}
        while(entrada.hasNext()&&i<largo){
            float elemento= entrada.nextFloat();
            res[i]=elemento;
            i++;
        }
        return res;
        
    }

    float[][] leerMatriz(Scanner entrada, int filas, int columnas) {
        float res[][];
        res=new float[filas][columnas];
        if(!entrada.hasNext()){return new float[0][0];}
        while(entrada.hasNext()){
           for(int i=0;i<filas;i++){
            for(int j=0;j<columnas;j++){
                float elemento=entrada.nextFloat();
                res[i][j]=elemento;
            }
           }
            
        }
        return res;
    }

    void imprimirPiramide(PrintStream salida, int alto) {
        String blank=" ";
        String asterisk="*";    
        for(int i=1;i<=alto;i++){
            int cantidadEspacios=alto-i;
            String imprimir=blank.repeat(cantidadEspacios)+asterisk.repeat(2*i-1)+blank.repeat(cantidadEspacios);
            salida.println(imprimir);
        }
    
    }
}
